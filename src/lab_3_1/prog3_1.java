package lab_3_1;

import java.util.*;
import static java.lang.System.out;

interface ableList<E>
{
	Object get(int index);
	int size();
	void add(E item);
	void remove(int index);
	void remove(E item);
	void clear();
}

class Fruits
{
	private String name;
	private double weight;

	Fruits(String aName, double aWeight)
	{
		this.name = aName;
		this.weight = aWeight;
	}

	String getInfo()
	{
		return "name: " + this.name + "\n weight: " + this.weight;
	}
}

class myArrayList<E> implements ableList<E>
{
	private ArrayList myList = new ArrayList<E>();

	public Object get(int index)
	{
		return this.myList.get(index);
	}

	public int size()
	{
		return this.myList.size();
	}

	public void add(E item)
	{
		this.myList.add(item);
	}

	public void remove(int index)
	{
		this.myList.remove(index);
	}

	public void remove(E item)
	{
		this.myList.remove(item);
	}

	public void clear()
	{
		this.myList.clear();
	}
}

class myLinkedList<E> implements ableList<E>
{
	private LinkedList myList = new LinkedList<E>();

	public Object get(int index)
	{
		return this.myList.get(index);
	}

	public int size()
	{
		return this.myList.size();
	}

	public void add(E item)
	{
		this.myList.add(item);
	}

	public void remove(int index)
	{
		this.myList.remove(index);
	}

	public void remove(E item)
	{
		this.myList.remove(item);
	}

	public void clear()
	{
		this.myList.clear();
	}
}

public class prog3_1
{
	public static void main(String args[])
	{
		//testing ArrayList
		out.println("start testing pocket...\n");
		myArrayList pocket = new myArrayList<Fruits>();
		pocket.add(new Fruits("apple", 1.5));
		pocket.add(new Fruits("pineapple", 3.0));
		Fruits ginger = new Fruits("ginger", 2.5);
		pocket.add(ginger);
//		pocket.clear();
//		pocket.remove(0);
//		pocket.remove(ginger);

		for(int i = 0; i < pocket.size(); i++)
		{
			Fruits bufferPocket = (Fruits)pocket.get(i);
			out.println(bufferPocket.getInfo());
		}
		out.println("\nend testing pocket...");

		//testing LinkedList
		out.println("start testing bag...\n");
		myLinkedList bag = new myLinkedList<Fruits>();
		bag.add(new Fruits("banana", 0.5));
		bag.add(new Fruits("strawberry", 2.0));
		Fruits mint = new Fruits("mint", 5.5);
		bag.add(mint);
//		bag.clear();
//		bag.remove(0);
//		bag.remove(mint);

		for(int i = 0; i < bag.size(); i++)
		{
			Fruits bufferBag = (Fruits)bag.get(i);
			out.println(bufferBag.getInfo());
		}
		out.println("\nend testing bag...");


	}
}
