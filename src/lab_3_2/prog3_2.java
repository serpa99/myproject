package lab_3_2;

import java.util.*;
import static java.lang.System.out;

abstract class Fruits extends Box
{
	abstract public double getFruitWeight();
	abstract public String getInfo();
}

class Apple extends Fruits
{
	private double weight = 1.0;

	@Override
        public String getInfo()
        {
                return "name: " + this.getClass().getName() + "\n weight: " + this.weight;
        }

	@Override
	public double getFruitWeight()
	{
		return this.weight;
	}

}

class Orange extends Fruits
{
	private double weight = 1.5;

	@Override
	public String getInfo()
        {
                return "name: " + this.getClass().getName() + "\n weight: " + this.weight;
        }

	@Override
	public double getFruitWeight()
	{
		return this.weight;
	}

}

class Box<Fruits>
{
	private ArrayList myList = new ArrayList<Fruits>();

	public void add(Fruits element)
	{
		this.myList.add(element);
	}

        public int size()
        {
                return this.myList.size();
        }

	public Object get(int i)
	{
		return this.myList.get(i);
	}

	public void clear()
	{
		this.myList.clear();
	}
}

public class prog3_2
{

        public static double getWeight(Box myList)
        {
                double localWeight = 0;

                for(int i = 0; i < myList.size(); i++)
                {
                        Fruits buffer = (Fruits)myList.get(i);
                        localWeight += buffer.getFruitWeight();
                }

                return localWeight;
        }

        public static boolean compare(Box thisList, Box anotherList)
        {
                return getWeight(thisList) == getWeight(anotherList);
        }

	public static void boxOutput(Box myBox)
	{
                for(int i = 0; i < myBox.size(); i++)
                {
                        Fruits bufferBox = (Fruits)myBox.get(i);
                        out.println("(" + (i+1) + ")" + bufferBox.getInfo());
                }

	}

	public static boolean boxFromTo(Box myBox, Box anotherBox)
	{
		Fruits bufferMyBox1 = (Fruits)myBox.get(0);
                Fruits bufferAnotherBox1 = (Fruits)anotherBox.get(0);

		if( myBox.get(0).getClass().getName() == anotherBox.get(0).getClass().getName())
		{

			out.println(bufferMyBox1.getClass().getName() + " == " + bufferMyBox1.getClass().getName());

			for(int i = 0; i < myBox.size(); i++)
			{
				Fruits bufferMyBox2 = (Fruits)myBox.get(i);

				anotherBox.add(bufferMyBox2);
			}

			myBox.clear();

			return true;
		}

		else
		{
			out.println( myBox.get(0).getClass().getName() + " != " + anotherBox.get(0).getClass().getName());
			return false;
		}
	}

	public static void main(String args[])
	{
		Box myBox = new Box<Apple>();
		Box anotherBox = new Box<Orange>();

		for(int i = 0; i < 2; i++)
		{
			myBox.add(new Apple());
			anotherBox.add(new Orange());
		}

		//first output
		out.println("\n" + myBox + " contains:");
		boxOutput(myBox);
		out.println("\n" + anotherBox + "contains:");
		boxOutput(anotherBox);

		out.println("\nbox " + myBox + " has weight: " + getWeight(myBox));
		out.println("\nbox " + anotherBox + " has weight: " + getWeight(anotherBox));

		out.println( "\ncomparing... " + compare(myBox, anotherBox) );

		out.println( "try to replace: " + boxFromTo(myBox, anotherBox) );

		//second output
                out.println("\n" + myBox + " contains:");
                boxOutput(myBox);
                out.println("\n" + anotherBox + "contains:");
                boxOutput(anotherBox);

                out.println("\nbox " + myBox + " has weight: " + getWeight(myBox));
                out.println("\nbox " + anotherBox + " has weight: " + getWeight(anotherBox));

                out.println( "\ncomparing... " + compare(myBox, anotherBox) );

	}
}
